import socket
from select import select
import struct
import argparse


def parse_icmp(icmp_data):
    pass


def get_args_parser():
    parser = argparse.ArgumentParser(description="Lying SNTP server.")
    parser.add_argument("source", help="Lying delay in seconds (default 0)")
    parser.add_argument("-p", "--port", help="Listening port (default 123)",
                        default=123, type=int)
    return parser


def main():
    parser = get_args_parser()
    args = parser.parse_args()
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP) as sniffer:
            sniffer.bind(('', 0))
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
                try:
                    sock.sendto(b'My Data.', (args.source, args.port))
                    print('Receiving')
                    read_sock, _, _ = select([sniffer], [], [], 3)
                    if read_sock:
                        data = sniffer.recv(1024)
                        print('Received')
                        a = struct.unpack("!BBHHH", data[:8])
                        print(a)
                    else:
                        print('Couldn\'t received')
                except Exception as e:
                    print(e)
    except PermissionError:
        print('Run it please by superuser to get access permissions.')


if __name__ == '__main__':
    main()
