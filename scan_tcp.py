from socket import socket, AF_INET, SOCK_STREAM
from select import select
from typing import Tuple


def read_sock_test(sock: socket) -> bool:
    pass


def scan_tcp_port(addr: str, port: int) -> Tuple[bool, str]:
    with socket() as sock:
        result = (True, '')
        sock.settimeout(0.1)
        try:
            sock.connect((addr, port))
            find_out_tcp_proto(addr, port)
        except:
            result = False

    return result


def find_out_tcp_proto(addr: str, port: int):
    # Проверяем TCP: HTTP, DNS, SMTP, POP3, IMAP
    pass


def find_out_udp_proto(addr: str, port: int):
    # Проверяем UDP: DNS, NTP
    pass


if __name__ == '__main__':
    print(scan_tcp_port('ya.ru', 81))
